﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using TableSplitting.Entities;

namespace TableSplitting
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new AppDbContext();

            context.Database.EnsureCreated();

            context.Persons.Add(new Person
            {
                Name = "Reza",

                Account = new Account
                {
                    AccountNumber = "786521"
                }
            });

            context.SaveChanges();

            // select Person & Account

            var personAccounts = context.Persons.Include(c=>c.Account).ToList();

            // select Person

            var persons = context.Persons.ToList();

            // select Account

            var accounts = context.Accounts.ToList();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using TableSplitting.Entities;

namespace TableSplitting
{
    public class AppDbContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<Account> Accounts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.;Database=SplitDb;Trusted_Connection=true;");
            
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().ToTable("Person");

            modelBuilder.Entity<Account>().ToTable("Person");

            modelBuilder.Entity<Person>().HasOne(c => c.Account)
                .WithOne(c => c.Person).HasForeignKey<Person>(p=>p.Id);
            
            base.OnModelCreating(modelBuilder);
        }
    }
}

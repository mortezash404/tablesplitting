﻿namespace TableSplitting.Entities
{
    public class Account
    {

        public int Id { get; set; }

        public string AccountNumber { get; set; }

        public Person Person { get; set; }
    }
}